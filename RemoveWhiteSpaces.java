package whiteSpaces;

import java.util.Scanner;

public class RemoveWhiteSpaces {
	public static String read() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the string: ");
		String input = scanner.nextLine();
		
		scanner.close();
		return input;
	}
	
	public static String removeWhiteSpaces(String str) {
		String stringWithoutSpaces = "";
		int stringLength = str.length();
		for (int counter = 0; counter < stringLength; counter++) {
			Character letter = str.charAt(counter);
			if(letter != ' ') {
				if(Character.isLetter(letter)) {
					stringWithoutSpaces += letter;
				}
			}
		}
		return stringWithoutSpaces;
	}
	
	public static void main(String[] args) {
		String inputString = read();
		String newString = removeWhiteSpaces(inputString);
		System.out.println(newString);
	}
}

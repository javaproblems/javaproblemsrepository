package word;

import java.util.Scanner;

public class LongestWord {
	public static String longestWord() {
		Scanner scanInput = new Scanner(System.in);
		System.out.println("Enter the number of words: ");
		int arrayLength = scanInput.nextInt(), maxLength = 0;
		String word = "";
		System.out.println("Enter the words: ");
		for (int counter = 0; counter < arrayLength; counter++) {
			String input = scanInput.next();
			int wordLength = input.length();
			if(wordLength > maxLength) {
				maxLength = wordLength;
				word = input;
			}
		}
		
		scanInput.close();
		return word;
	}
	
	public static void main(String[] args) {
		String word = longestWord();
		System.out.println("The longest word is: " + word);
	}

}

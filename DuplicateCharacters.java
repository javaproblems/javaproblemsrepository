package duplicates;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DuplicateCharacters {
	
	public static String readString() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the string: ");
		String input = scanner.next();
		
		scanner.close();
		
		return input;
	}
	
	public static void duplicates(String word) {
		Map<Character, Integer> map = new HashMap<>();
		
		for (int counter = 0; counter < word.length(); counter++) {
			Character letter = word.charAt(counter);
			if(map.containsKey(letter)) {
				map.put(letter, map.get(letter) + 1);
			} else {
				map.put(letter, 1);
			}
		}
		
		for (Map.Entry<Character, Integer> elements : map.entrySet()) {
			if(elements.getValue() > 1) {
				System.out.println("The letter " + elements.getKey() + " appears " + elements.getValue() + " times.");
			}
		}
	}

	public static void main(String[] args) {
		String input = readString();
		
		duplicates(input);
	}

}

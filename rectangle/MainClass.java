package rectangle;

public class MainClass {

	public static void main(String[] args) {
		Rectangle rectangle = new Rectangle(4, 6);
		System.out.println("Perimeter is: " + rectangle.perimeter());
		System.out.println("Area is: " + rectangle.area());
		if(rectangle.isSquare()) {
			System.out.println("The rectangle is square.");
		} else {
			System.out.println("The rectangle isn't square.");
		}
	}
}

package rectangle;

public class Rectangle {
	private int width, length;

	public Rectangle(int width, int length) {
		this.width = width;
		this.length = length;
	}
	
	public int perimeter() {
		return 2 * (width + length);
	}
	
	public int area() {
		return width * length;
	} 
	
	public boolean isSquare() {
		boolean boolSquare = false;
		if(width == length) {
			boolSquare = true;
		}
		return boolSquare;
	 } 
}

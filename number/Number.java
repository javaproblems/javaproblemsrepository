package number;

public class Number {
	private int nr;

	public Number(int nr) {
		this.nr = nr;
	}

	public int getNr() {
		return nr;
	}

	public void divisors() {
		for(int counter = 1; counter <= nr/2; counter++) {
			if(nr % counter == 0) {
				System.out.print(counter + " ");
			}
		}
		System.out.println(nr);
	}
	
	public boolean isPrime() {
		boolean boolPrime = true;
		for(int counter = 2; counter <= nr/2; counter++) {
			if(nr % counter == 0){
				boolPrime = false;
				break;
			}
		}
		return boolPrime;
	}
	
	public boolean isSquareNumber() {
		boolean boolSquare = true;
		int radical = (int) Math.sqrt(nr);
		if(radical * radical != nr) {
			boolSquare = false;
		}
		
		return boolSquare;
	}
}

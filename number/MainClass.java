package number;

public class MainClass {

	public static void main(String[] args) {
		Number number = new Number(36);
		System.out.println("Divisors of  " + number.getNr());
		number.divisors();
		
		if(number.isSquareNumber()) {
			System.out.println("The number is square number.");
		} else {
			System.out.println("The number isn't square number.");
		}
		
		if(number.isPrime()) {
			System.out.println("The number is prime.");
		}
		else {
			System.out.println("The number isn't prime."); 
		}
	}

}

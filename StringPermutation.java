package permutation;

import java.util.Scanner;

public class StringPermutation {
	public static String readString() {
		Scanner scanInput = new Scanner(System.in);
		System.out.println("Enter the string: ");
		String input = scanInput.next();
		
		scanInput.close();
		
		return input;
	}
	
	public static void stringPermutations(String inputString, String newString) {
		if(inputString.length() == 0) {
			System.out.println(newString);
		} else {
			for(int counter = 0; counter < inputString.length(); counter++) {
				String str = newString + inputString.charAt(counter);
				String newInputString = inputString.substring(0, counter) + inputString.substring(counter+1);
				stringPermutations(newInputString, str);
			}
		}
	}

	public static void main(String[] args) {
		String word = readString();
		stringPermutations(word, "");
	}

}

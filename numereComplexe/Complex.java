package numereComplexe;

public class Complex {
	 private double real;
	 private double imaginar;
	 
	public Complex(double real, double imaginar) {
		this.real = real;
		this.imaginar = imaginar;
	}

	public double getReal() {
		return real;
	}

	public double getImaginar() {
		return imaginar;
	}
	 
	public double modul() {
		return Math.sqrt(real * real + imaginar * imaginar);
	} 
	
	public void suma(Complex c) {
		real = real + c.real;
		imaginar = imaginar + c.imaginar;
	} 
	
	 public void produs(Complex c) {
		 real = real * c.real-imaginar * c.imaginar;
		 imaginar = real * c.imaginar + imaginar * c.real;
	 }

	public String toString() {
		return real + " " + imaginar;
	} 
}

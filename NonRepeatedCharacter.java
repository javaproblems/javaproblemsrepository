package repeatedCharacters;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.management.RuntimeErrorException;

public class NonRepeatedCharacter {
	public static String readString() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the string: ");
		String input = scanner.next();
		
		scanner.close();
		
		return input;
	}
	
	public static char FirstNonRepeatedCharacter(String str) {
		Map<Character, Integer> map = new HashMap<>();
		
		for (int counter = 0; counter < str.length(); counter++) {
			Character character = str.charAt(counter);
			if(map.containsKey(character)) {
				map.put(character, map.get(character) + 1);
			} else {
				map.put(character, 1);
			}
		}
		
		for (Map.Entry<Character, Integer> mapElements : map.entrySet()) {
			if(mapElements.getValue() == 1) {
				return mapElements.getKey();
			}
		}
		throw new RuntimeErrorException(null, "It doesn't exist non repeated character");
	}

	public static void main(String[] args) {
		String inputString = readString();
		char firstChar = FirstNonRepeatedCharacter(inputString);
		System.out.println("The first non repeated character is " + firstChar);
	}

}

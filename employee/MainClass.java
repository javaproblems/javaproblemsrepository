package employee;

public class MainClass {
	public static void main(String[] args) {
		Employee emp = new Employee("Andrei", 25, 3200);
		if(emp.getSalary() < 3500) {
			emp.setSalary(3500);
		}
		emp.print();
		
		Programmer programmer = new Programmer("Ioana", 30, 4500, "Java");
		programmer.print();
	}
}

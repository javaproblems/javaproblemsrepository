package employee;

public class Programmer extends Employee{
	private String language;


	public Programmer(String name, int age, double salary, String language) {
		super(name, age, salary);
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}


	public void setLanguage(String language) {
		this.language = language;
	}
	
	public void print() {
		super.print();
		System.out.println("Language: " + this.language);
	}
}

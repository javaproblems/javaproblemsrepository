package duplicates;

import java.util.ArrayList;

public class ArrayDuplicates {
	
	public static ArrayList<Object> initialize() {
		ArrayList<Object> array = new ArrayList<Object>();
		
		array.add("c");
		array.add("b");
		array.add("a");
		array.add("b");
		array.sort(null);
		
		return array;
	}
	
	public static ArrayList<Object> removeDuplicates(ArrayList<Object> array) {
		int length;
		ArrayList<Object> newArray = new ArrayList<Object>();
		
		length = array.size();
		
		newArray.add(array.get(0));
		
		for(int counter = 1; counter < length; counter++) {
			if(array.get(counter - 1) != array.get(counter)) {
				newArray.add(array.get(counter));
			}
		}
		
		return newArray;
	}

	public static void main(String[] args) {
		ArrayList<Object> array = initialize();
		System.out.println(array);
		array = removeDuplicates(array);
		System.out.println(array);
	}
}

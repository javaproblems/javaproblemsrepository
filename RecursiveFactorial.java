package factorial;

import java.util.Scanner;

public class RecursiveFactorial {
	
	public static int readNumber() {
		System.out.println("Enter the number: ");
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		
		scanner.close();
		
		return number;
	}
	
	public static long factorial(int number) {
		if(number == 1) {
			return 1;
		} else {
			return number * factorial(number - 1);
		}
	}

	public static void main(String[] args) {
		int number = readNumber();
		long recursiveFactorialResult = factorial(number);
		
		System.out.println("The result is " + recursiveFactorialResult);
	}

}

package armstrongNumber;

import java.util.Scanner;

public class ArmstrongNumber {
	
	public static int readNumber() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number: ");
		int number = scanner.nextInt();
		scanner.close();
		
		return number;
	}
	
	public static boolean isArmstrongNumber(int number) {
		int sum = 0, numberCopy = number;
		
		while(numberCopy != 0) {
			sum += Math.pow(numberCopy % 10, 3);
			numberCopy /= 10;
		}
		
		if(sum != number) {
			return false;
		}
		
		return true;
	}

	public static void main(String[] args) {
		int armstrongNumber = readNumber();
		
		if(isArmstrongNumber(armstrongNumber)) {
			System.out.println("The number entered is Armstrong.");
		} else {
			System.out.println("The number entered isn't Armstrong.");
		}
	}
}

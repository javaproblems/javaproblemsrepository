package person;

import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {
		final int arrayLength = 4;
		Student studentArray[] = new Student[arrayLength];
		
		Scanner scanner = new Scanner(System.in);
		for (int counter = 0; counter < arrayLength; counter++) {
			System.out.println("Name: ");
			String studentName = scanner.next();
			System.out.println("Age: ");
			int studentAge = scanner.nextInt();
			System.out.println("Faculty: ");
			String studentFaculty = scanner.next();
			studentArray[counter] = new Student(studentName, studentAge, studentFaculty);
		}
		scanner.close();
		
		int counter = 0;
		for (int index = 0; index < arrayLength; index++) {
			String name = studentArray[index].getName();
			String faculty = studentArray[index].getFaculty();
			if((name.compareTo("Alex") == 0) && (faculty.compareTo("UVT") == 0)) {
				counter++;
			}
		}
		
		System.out.println("There are " + counter + " students named Alex who are studying at the UVT.");
	}
}

package person;

public class Student extends Person{
	private String faculty;

	public Student(String name, int age, String faculty) {
		super(name, age);
		this.faculty = faculty;
	}

	public String getFaculty() {
		return faculty;
	}
}

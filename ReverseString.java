package reverse;

import java.util.Scanner;

public class ReverseString {
	public static String read() {
		Scanner scanInput = new Scanner(System.in);
		System.out.println("Enter the string: ");
		String strVar = scanInput.nextLine();

		scanInput.close();
		return strVar;
	}

	public static StringBuilder reverseString(String str, StringBuilder reverseString) {
		for(int counter = str.length() - 1; counter >= 0; counter--) {
			if(Character.isLetter(str.charAt(counter))) {
				reverseString.append(str.charAt(counter));
			}
		}
		return reverseString;
	}

	public static void main(String[] args) {
		String stringVar = read();
		StringBuilder newString = new StringBuilder();
		StringBuilder reversedString = reverseString(stringVar, newString);


		System.out.println("The new string is: " + reversedString);
	}
}

package angle;

public class MainClass {

	public static void main(String[] args) {
		Angle angle = new Angle(60);
		if(angle.isComplementary(new Angle(30))) {
			System.out.println("These angles are complementary.");
		} else {
			System.out.println("These angles aren't complementary.");
		}
		
		System.out.println("The value of the angle in radians is: " + angle.radiansConversion());
	}

}

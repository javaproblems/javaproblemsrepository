package angle;

public class Angle {
	private int angleDegrees;
		
	public Angle(int angleDegrees) {
		this.angleDegrees = angleDegrees;
	}

	public boolean isComplementary(Angle ang) {
		boolean boolComplementary = false;
		if(this.angleDegrees + ang.angleDegrees == 90) {
			boolComplementary = true;
		}
		return boolComplementary;
	}

	public double radiansConversion(){
		return (Math.PI * angleDegrees) / 180;
	}
}

package reverse;

import java.util.Scanner;

public class Reverse {
	
	public static int read() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the number: ");
		int number = scan.nextInt();
		
		scan.close();
		return number;
	}
	
	public static int reverse(int num) {
		int newNumber = 0;
		
		while(num != 0) {
			newNumber = newNumber * 10 + num % 10;
			num /= 10;
		}
		
		return newNumber;
	}

	public static void main(String[] args) {
		int number = read();
		int reverseNumber = reverse(number);
		
		System.out.println(reverseNumber);
	}

}

package reverseArray;

import java.util.Scanner;

public class ReverseArray {
	public static String[] read() {
		Scanner scanInput = new Scanner(System.in);
		System.out.println("Enter the number of elements: ");
		int arrayLength = scanInput.nextInt();
		String elements[] = new String[arrayLength];
		System.out.println("Enter the elements: ");
		for (int counter = 0; counter < arrayLength; counter++) {
			elements[counter] = scanInput.next();
		}
		scanInput.close();
		return elements;
	}
	
	public static void reverseArray(String elem[]) {
		int elemLength = elem.length;
		if(elemLength <= 1) {
			System.out.println("The array can't be reversed");
		} else {
			String aux;
			for (int counter = 0; counter < elemLength / 2; counter++) {
				aux = elem[counter];
				elem[counter] = elem[elemLength - counter - 1];
				elem[elemLength - counter - 1] = aux;
			}
		}
	}
	
	public static void print(String elem[]) {
		int elemLength = elem.length;
		for (int counter = 0; counter < elemLength; counter++) {
			System.out.println(elem[counter] + " ");
		}
	}

	public static void main(String[] args) {
		String elements[] = read();
		reverseArray(elements);
		print(elements);
	}
}

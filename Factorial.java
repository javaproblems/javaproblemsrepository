package factorial;

import java.util.Scanner;

public class Factorial {
	
	public static int readNumber() {
		System.out.println("Enter the number: ");
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		
		scanner.close();
		
		return number;
	}
	
	public static long factorial(int number) {
		long product = 1;
		
		for (int counter = 1; counter <= number; counter++) {
			product *= counter;
		}
		
		return product;
	}

	public static void main(String[] args) {
		int number = readNumber();
		long result = factorial(number);
		
		System.out.println("The factorial of the number " + number + " is " + result);
	}

}

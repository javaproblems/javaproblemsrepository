package sort;

import java.util.Scanner;

public class SelectionSort {
	public static int[] read() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the array length: ");
		int arrayLength = scanner.nextInt();
		int elemArray[] = new int[arrayLength];
		System.out.println("Enter the array elements: ");
		for (int counter = 0; counter < arrayLength; counter++) {
			elemArray[counter] = scanner.nextInt();
		}

		scanner.close();
		return elemArray;
	}

	public static void selectionSort(int elementsArr[]) {
		int elementsArrLength = elementsArr.length;
        for (int firstCounter = 0; firstCounter < elementsArrLength-1; firstCounter++)
        {
            int indexMin = firstCounter;
            for (int secondCounter = firstCounter+1; secondCounter < elementsArrLength; secondCounter++) {
            	if (elementsArr[indexMin] > elementsArr[secondCounter]) {
            		indexMin = secondCounter;
            	}
            }

            int aux = elementsArr[indexMin];
            elementsArr[indexMin] = elementsArr[firstCounter];
            elementsArr[firstCounter] = aux;
        }
	}

	public static void print(int[] elements) {
		int elementsLength = elements.length;
		for (int counter = 0; counter < elementsLength; counter++) {
			System.out.print(elements[counter] + " ");
		}
	}

	public static void main(String[] args) {
		int elementsArray[] = read();
		selectionSort(elementsArray);
		print(elementsArray);
	}
}

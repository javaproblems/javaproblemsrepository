package year;

import java.util.Scanner;

public class LeapYear {
	public static int read() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the year: ");
		String numberAsString = scan.next();
		if(numberAsString.length() != 4) {
			System.out.println("The number doesn't have 4 digits.");
			System.exit(0);
		}
		int number = Integer.parseInt(numberAsString);
		
		scan.close();
		return number;
	}
	
	public static boolean isLeapYear(int year) {
		boolean leap = false;
		if((year % 100 != 0) && (year % 4 == 0)) {
			leap = true;
		} else {
			leap = false;
		}
		return leap;
	}

	public static void main(String[] args) {
		try {
			int year = read();
			boolean leapYearVariable = isLeapYear(year);
			
			if(leapYearVariable) {
				System.out.println("The year " + year + " is leap year.");
			} else {
				System.out.println("The year " + year + " isn't leap year.");
			}
		} 
		catch(Exception exception) {
			exception.printStackTrace();
		}
		
	}
}

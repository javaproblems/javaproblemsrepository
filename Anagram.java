package anagram;

import java.util.Scanner;

public class Anagram {
	
	public static String readString() {
		Scanner scanInput = new Scanner(System.in);
		
		System.out.println("Enter the first string: ");
		String firstString = scanInput.next();
		
		System.out.println("Enter the second string: ");
		String secondString = scanInput.next();
		
		scanInput.close();
		
		return firstString + " " + secondString;
	}
	
	public static boolean anagram(String firstString, String secondString) {
		
		int countFirstString[] = new int[256], countSecondString[] = new int[256];
		
		
		if(firstString.length() != secondString.length()) {
			return false;
		} else {	
			for (int i = 0; i < firstString.length() && i < secondString.length(); i++) {
				countFirstString[firstString.charAt(i)]++;
				countSecondString[secondString.charAt(i)]++;
			}
			
			for (int i = 0; i < 256; i++) {
				if(countFirstString[i] != countSecondString[i]) {
					return false;
				}
			}
		}
		
		return true;
	}

	public static void main(String[] args) {
		String[] input = readString().split(" ");
		
		String firstValue = input[0].toLowerCase();
		String secondValue = input[1].toLowerCase();
		
		if(anagram(firstValue, secondValue)) {
			System.out.println("The two strings are anagrams!");
		} else {
			System.out.println("The two strings aren't anagrams!");
		}
	}
}

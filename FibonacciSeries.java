package fibonacci;

import java.util.Scanner;

public class FibonacciSeries {
	
	public static int readNumber() {
		Scanner scanInput = new Scanner(System.in);
		System.out.println("Enter the number: ");
		int number = scanInput.nextInt();
		
		scanInput.close();
		
		return number;
	}
	
	public static void printFibonacciSeries(int limit) {
		int prev = 0, next = 1, sum = 0;
		
		System.out.print(prev + " " + next + " ");
		for(int counter = 2; counter < limit; counter++) {
			sum = prev + next;
			prev = next;
			next = sum;
			System.out.print(sum + " ");
		}
	}

	public static void main(String[] args) {
		int number = readNumber();
		
		printFibonacciSeries(number);
	}

}

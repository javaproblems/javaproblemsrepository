package sort;

import java.util.Scanner;

public class InsertionSort {
	public static int[] read() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the array length: ");
		int arrayLength = scanner.nextInt();
		int elemArray[] = new int[arrayLength];
		System.out.println("Enter the array elements: ");
		for (int counter = 0; counter < arrayLength; counter++) {
			elemArray[counter] = scanner.nextInt();
		}

		scanner.close();
		return elemArray;
	}

	public static void insertionSort(int elementsArr[]) {
		int elementsArrLength = elementsArr.length;
		for (int firstCounter = 1; firstCounter < elementsArrLength; firstCounter++) {
			int currentElement = elementsArr[firstCounter];
			int secondCounter = firstCounter - 1;
			while((secondCounter >= 0) && (elementsArr[secondCounter] > currentElement)) {
				elementsArr[secondCounter + 1] = elementsArr[secondCounter];
				secondCounter--;
			}
			elementsArr[secondCounter + 1] = currentElement;
		}
	}

	public static void print(int[] elementsArr) {
		int elementsArrLength = elementsArr.length;
		for (int counter = 0; counter < elementsArrLength; counter++) {
			System.out.print(elementsArr[counter] + " ");
		}
	}

	public static void main(String[] args) {
		int elementsArray[] = read();
		insertionSort(elementsArray);
		print(elementsArray);
	}
}

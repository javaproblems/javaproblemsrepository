package pyramidPattern;

import java.util.Scanner;

public class Pattern {
	
	public static int readInput() {
		System.out.println("Enter the number of pyramid levels: ");
		Scanner scanInput = new Scanner(System.in);
		int pyramidLevels = scanInput.nextInt();
		
		scanInput.close();
		
		return pyramidLevels;
	}
	
	public static void drawPyramid(int number) {
		int num = number;
		
		for (int firstCounter = 0; firstCounter <= number; firstCounter++) {
			for (int blankSpacesCounter = 0; blankSpacesCounter < num; blankSpacesCounter++) {
				System.out.print(" ");
			}
			
			num--;
			
			for (int secondCounter = 0; secondCounter < firstCounter; secondCounter++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		int number = readInput();
		
		drawPyramid(number);
	}
}
